##################################################################################
# Copyright (C) 2020 Isaac Chun Fung Wong
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##################################################################################

##
# @file gwcomp_residual_pipe.py
# @author Isaac Chun Fung Wong
# @brief This program generates A DAG file for generating residual frames.

from __future__ import print_function
import argparse
from string import Template

# Template for BW run.

bw_res_bash_template = """#!/bin/bash
source /home/sudarshan.ghonge/opt/bayeswave_master/etc/bayeswave-user-env.sh
cmdline=\"bayeswave_pipe -r $workdir --trigger-time $trigtime --skip-datafind --osg-deploy --copy-frames $config\"

echo \"Executing:\"
echo \"$${cmdline}\"
eval \"$${cmdline}\"
condor_submit_dag $workdir/$rundir.dag

"""
bw_res_bash_template = Template(bw_res_bash_template)


bw_event_bash_template = """#!/bin/bash
source /home/sudarshan.ghonge/opt/bayeswave_master/etc/bayeswave-user-env.sh

cmdline=\"bayeswave_pipe -r $workdir --trigger-time $trigtime --osg-deploy $extra $config\"
echo \"Executing:\"
echo \"$${cmdline}\"
eval \"$${cmdline}\"
condor_submit_dag $workdir/$rundir.dag

"""
bw_event_bash_template = Template(bw_event_bash_template)

def parser():
	parser = argparse.ArgumentParser(description="This helper program parses info from config file and save to txt.")
	parser.add_argument("--get",type=str,help="Path to generate a config file.",required=False)
	parser.add_argument("--config",type=str,help="Path to a completed config file.",required=False)
	parser.add_argument("--submit",help="Submit the job",action="store_true")
	return parser

def main(args=None):
	import ast
	from ligo.gracedb.rest import GraceDb
	import os, stat
	import glob
	from pycondor import Dagman,Job
	import pycondor.utils
	from configparser import ConfigParser
	from termcolor import cprint
	import subprocess
	import shlex
	import json
	import numpy as np
	from shutil import copyfile

	opts = parser().parse_args(args)

	if opts.get is not None:
		config = ConfigParser(allow_no_value=True)
		config.optionxform = str
		config.add_section("condor")
		config.set("condor","accounting-group-user",os.popen("whoami").read().strip())
		config.set("condor","accounting-group","ligo.prod.o3.cbc.testgr.tiger")
		config.set("condor","universe","vanilla")
		config.add_section("residual")
		config.set("residual","request-memory","8GB")
		config.set("residual","request-disk","8GB")
		config.set("residual","# List of super-events to process")
                config.set("residual","# Superevent IDs separated by space.")
		config.set("residual","superevents","")
                config.add_section("bayeswave")
                config.set("bayeswave","accounting-group","ligo.prod.o3.burst.paramest.bayeswave")
                config.set("bayeswave","request-memory","4000")
                config.set("bayeswave","post-request-memory","8000")
                config.set("bayeswave","bayeswave",os.popen("which BayesWave").read().strip())
                config.set("bayeswave","bayeswave_post",os.popen("which BayesWavePost").read().strip())
                config.set("bayeswave","megaplot",os.popen("which megaplot.py").read().strip())
                config.set("bayeswave","megasky",os.popen("which megasky.py").read().strip())

		with open(opts.get,"w") as f:
			config.write(f)
	elif opts.config is not None:
		config = ConfigParser()
		config.optionxform = str
		config.read(opts.config)

		ISSUES = {"missing-PEs":[]}

		error = os.path.abspath("condor/error")
		output = os.path.abspath("condor/output")
		log = os.path.abspath("condor/log")
		submit = os.path.abspath("condor/submit")
		extra_lines = [
				"accounting_group_user = %s"%config["condor"]["accounting-group-user"],
				"accounting_group = %s"%config["condor"]["accounting-group"],
				"stream_error = True",
				"stream_output = True"]
		dag_res = Dagman(name="residual_pipe",submit=submit)
		superevents = config["residual"]["superevents"].split()

		srate_max = 2048
		seglen_max = 8
		pe_dir = "/home/sudarshan.ghonge/Projects/O3_PE/o3a_catalog_events"
		pe_data_dir = "/home/sudarshan.ghonge/Projects/O3a/C01_runs"
		# Assumed directory to store config files of preferred runs.
		pwd = os.getcwd()
		rundirs = []
		config_bw_event = ConfigParser()
                config_bw_event.optionxform = str
                config_bw_event.add_section("input")
                config_bw_event.set("input","dataseed","1234")
                config_bw_event.set("input","seglen","")
                config_bw_event.set("input","window","1.0")
                config_bw_event.set("input","flow","20")
                config_bw_event.set("input","srate", "")
                config_bw_event.set("input","PSDlength","")
                config_bw_event.set("input","padding","0.0")
                config_bw_event.set("input","keep-frac","1.0")
                config_bw_event.set("input","rho-threshold","7.0")
                config_bw_event.set("input","ifo-list","")
                config_bw_event.add_section("engine")
                config_bw_event.set("engine","bayeswave",config["bayeswave"]["bayeswave"])
                config_bw_event.set("engine","bayeswave_post",config["bayeswave"]["bayeswave_post"])
                config_bw_event.set("engine","megaplot",config["bayeswave"]["megaplot"])
                config_bw_event.set("engine","megasky",config["bayeswave"]["megasky"])
                config_bw_event.set("engine","singularity","\"/cvmfs/singularity.opensciencegrid.org/lscsoft/bayeswave:v1.0.5\"")
                config_bw_event.add_section("datafind")
                config_bw_event.set("datafind","frtype-list","")
                config_bw_event.set("datafind","channel-list","")
                config_bw_event.set("datafind","url-type","file")
                config_bw_event.set("datafind","veto-categories","[1]")
                config_bw_event.add_section("bayeswave_options")
                config_bw_event.set("bayeswave_options","updateGeocenterPSD","")
                config_bw_event.set("bayeswave_options","waveletPrior","")
                config_bw_event.set("bayeswave_options","Dmax","100")
                config_bw_event.set("bayeswave_options","signalOnly","")
                config_bw_event.set("bayeswave_options", "bayesLine", "")
                config_bw_event.add_section("bayeswave_post_options")
                config_bw_event.set("bayeswave_post_options","0noise","")
                config_bw_event.set("bayeswave_post_options", "bayesLine", "")
                config_bw_event.add_section("background")
                config_bw_event.set("background","events","all")
                config_bw_event.add_section("condor")
                config_bw_event.set("condor","universe",config["condor"]["universe"])
                config_bw_event.set("condor","checkpoint","")
                config_bw_event.set("condor","bayeswave-request-memory",config["bayeswave"]["request-memory"])
                config_bw_event.set("condor","bayeswave_post-request-memory",config["bayeswave"]["post-request-memory"])
                config_bw_event.set("condor","datafind","/usr/bin/gw_data_find")
                config_bw_event.set("condor","ligolw_print","/usr/bin/ligolw_print")
                config_bw_event.set("condor","segfind","/usr/bin/ligolw_segment_query_dqsegdb")
                config_bw_event.set("condor","accounting-group",config["bayeswave"]["accounting-group"])
                config_bw_event.set("condor","accounting-group-user",os.popen("whoami").read().strip())
                config_bw_event.add_section("segfind")
                config_bw_event.set("segfind","segment-url","https://segments.ligo.org")
                config_bw_event.add_section("segments")
                config_bw_event.set("segments","h1-analyze","H1:DMT-ANALYSIS_READY:1")
                config_bw_event.set("segments","l1-analyze","L1:DMT-ANALYSIS_READY:1")
                config_bw_event.set("segments","v1-analyze","V1:ITF_SCIENCEMODE")



		config_bw_res = ConfigParser()
                config_bw_res.optionxform = str
                config_bw_res.add_section("input")
                config_bw_res.set("input","dataseed","1234")
                config_bw_res.set("input","seglen","")
                config_bw_res.set("input","window","1.0")
                config_bw_res.set("input","flow","20")
                config_bw_res.set("input","srate", "")
                config_bw_res.set("input","PSDlength","")
                config_bw_res.set("input","padding","0.0")
                config_bw_res.set("input","keep-frac","1.0")
                config_bw_res.set("input","rho-threshold","7.0")
                config_bw_res.set("input","ifo-list","")
                config_bw_res.add_section("engine")
                config_bw_res.set("engine","bayeswave",config["bayeswave"]["bayeswave"])
                config_bw_res.set("engine","bayeswave_post",config["bayeswave"]["bayeswave_post"])
                config_bw_res.set("engine","megaplot",config["bayeswave"]["megaplot"])
                config_bw_res.set("engine","megasky",config["bayeswave"]["megasky"])
                config_bw_res.set("engine","singularity","\"/cvmfs/singularity.opensciencegrid.org/lscsoft/bayeswave:v1.0.5\"")
                config_bw_res.add_section("datafind")
                config_bw_res.set("datafind","frtype-list","")
                config_bw_res.set("datafind","channel-list","")
                config_bw_res.set("datafind","url-type","file")
		config_bw_res.set("datafind", "ignore-science-segments", "True")
                config_bw_res.set("datafind","veto-categories","[1]")
                config_bw_res.add_section("bayeswave_options")
                config_bw_res.set("bayeswave_options","updateGeocenterPSD","")
                config_bw_res.set("bayeswave_options","waveletPrior","")
                config_bw_res.set("bayeswave_options","Dmax","100")
                config_bw_res.set("bayeswave_options","signalOnly","")
                config_bw_res.set("bayeswave_options", "bayesLine", "")
                config_bw_res.add_section("bayeswave_post_options")
                config_bw_res.set("bayeswave_post_options","0noise","")
                config_bw_res.set("bayeswave_post_options", "bayesLine", "")
                config_bw_res.add_section("background")
                config_bw_res.set("background","events","all")
                config_bw_res.add_section("condor")
                config_bw_res.set("condor","universe",config["condor"]["universe"])
                config_bw_res.set("condor","checkpoint","")
                config_bw_res.set("condor","bayeswave-request-memory",config["bayeswave"]["request-memory"])
                config_bw_res.set("condor","bayeswave_post-request-memory",config["bayeswave"]["post-request-memory"])
                config_bw_res.set("condor","datafind","/usr/bin/gw_data_find")
                config_bw_res.set("condor","ligolw_print","/usr/bin/ligolw_print")
                config_bw_res.set("condor","segfind","/usr/bin/ligolw_segment_query_dqsegdb")
                config_bw_res.set("condor","accounting-group",config["bayeswave"]["accounting-group"])
                config_bw_res.set("condor","accounting-group-user",os.popen("whoami").read().strip())
                config_bw_res.add_section("segfind")
                config_bw_res.set("segfind","segment-url","https://segments.ligo.org")
                config_bw_res.add_section("segments")
                config_bw_res.set("segments","h1-analyze","H1:DMT-ANALYSIS_READY:1")
                config_bw_res.set("segments","l1-analyze","L1:DMT-ANALYSIS_READY:1")
                config_bw_res.set("segments","v1-analyze","V1:ITF_SCIENCEMODE")


		bw_run_f = open("bayeswave_run.sh","w")
		bw_run_f.write("#!/bin/bash\n\n")
		user = os.popen("whoami").read().strip()
		prod_list = np.loadtxt(os.path.join(pe_data_dir, "prod_list.txt"), dtype=str)
		prod_dict = {prod_list[x,0]:prod_list[x,1] for x in range(len(prod_list))}
		for event in superevents:
			cprint("Processing superevent %s..."%event, "yellow")
			prod_run = prod_dict[event]
			cprint("Prod run: %s"%(prod_run))
			if not os.path.isdir(event):
				os.mkdir(event)
			# Locate the config files.
			pe_file = glob.glob(os.path.join(pe_data_dir, event, "PE", "*.dat"))
			print(pe_file)
			if len(pe_file)==0:
				cprint("ERROR: no PE file(s) found for %s. Please contact owner of the catalog directory."%event,"red")
				ISSUES["missing-PEs"].append(event)
				continue
			pe_file = pe_file[0]
			# 
			pe_config_file  = glob.glob(os.path.join(pe_dir, event, 'C01_offline', '%s*.ini'%(prod_run)))
			approx = 'IMRPhenomPv2' # This is prod0
                        config_pe = ConfigParser()
                        config_pe.optionxform = str
                        config_pe.read(pe_config_file)
                        ifos = ast.literal_eval(config_pe["analysis"]["ifos"])
			
			trigtime_file = glob.glob(os.path.join(pe_dir, event, "C01_offline", "*gps*.txt".format(event)))
                        trigtime = np.asscalar(np.loadtxt(trigtime_file[0]))

			event_dir = os.path.abspath(event)
			rundir = os.path.abspath(os.path.join(event,'%s'%prod_run))
			if not os.path.isdir(rundir):
				os.mkdir(rundir)
                        seglen = config_pe.getfloat("engine","seglen")
                        if seglen > seglen_max:
                                cprint("WARNING: seglen (%s) in PE config is greater than the default maximum (%s). seglen is set to the default maximum value."%(seglen,seglen_max),"red")
                                seglen = seglen_max
                        srate = config_pe.getfloat("engine","srate")
                        if srate>srate_max:
                                cprint("WARNING: srate (%s) in PE config is greater than the default maximum (%s). srate is set to the default maximum value."%(srate,srate_max),"red")
                                srate = srate_max
                        flows = ast.literal_eval(config_pe["lalinference"]["flow"])
                        flows = {ifo:float(flows[ifo]) for ifo in ifos}
                        flow = np.max(flows.values())

                        frames = ast.literal_eval(config_pe["datafind"]["types"])
                        frames = {ifo:frames[ifo] for ifo in ifos}
                        
			channels = ast.literal_eval(config_pe["data"]["channels"])
                        channels = {ifo:channels[ifo] for ifo in ifos}
			channels_arr = [channels[ifo] for ifo in ifos]

			# Config for bw event run
                        config_bw_event.set("input","seglen",str(seglen))
                        config_bw_event.set("input","ifo-list",str(ifos))
                        config_bw_event.set("input","PSDlength",str(seglen))
                        config_bw_event.set("datafind","frtype-list",str(frames))
                        config_bw_event.set("datafind","channel-list",str(channels))
                        config_bw_event.set("input","srate",str(srate))
                        config_bw_event.set("input", "flow", str(flow))

			# Config for bw residual run
			config_bw_res.set("input","seglen",str(seglen))
                        config_bw_res.set("input","ifo-list",str(ifos))
                        config_bw_res.set("input","PSDlength",str(seglen))
			frames_res = {ifo:'{}_HOFT_RES'.format(ifo) for ifo in ifos}
                        config_bw_res.set("datafind","frtype-list",str(frames_res))
			channels_res = {ifo:'{}:residual'.format(ifo) for ifo in ifos}
                        config_bw_res.set("datafind","channel-list",str(channels_res))
                        config_bw_res.set("input","srate",str(srate))
                        config_bw_res.set("input", "flow", str(flow))

			if(seglen*srate>2048.0*4.0):
                                config_bw_event.set("condor","bayeswave-request-memory",str(float(config["bayeswave"]["request-memory"])*seglen*srate/(2048.*4.0)))
                                config_bw_event.set("condor","bayeswave_post-request-memory",str(float(config["bayeswave"]["post-request-memory"])*seglen*srate/(2048.*4.0)))
				
				config_bw_res.set("condor","bayeswave-request-memory",str(float(config["bayeswave"]["request-memory"])*seglen*srate/(2048.*4.0)))
                                config_bw_res.set("condor","bayeswave_post-request-memory",str(float(config["bayeswave"]["post-request-memory"])*seglen*srate/(2048.*4.0)))

			cache_dir = os.path.abspath(os.path.join(rundir,"cache"))
			if not os.path.isdir(cache_dir):
				os.mkdir(cache_dir)
			residual_dir = os.path.abspath(os.path.join(rundir,"residual_frame"))
			if not os.path.isdir(residual_dir):
				os.mkdir(residual_dir)
			bw_event_dir = os.path.abspath(os.path.join(rundir,"%s_bayeswave_event"%event))
			bw_res_dir = os.path.abspath(os.path.join(rundir,"%s_bayeswave_residual"%event))
			bw_res_cache_dir = os.path.join(rundir,"res_cache")
			if not os.path.isdir(bw_res_cache_dir):
				os.mkdir(bw_res_cache_dir)
			# Check whether cache are present in catalog directory.
			if os.path.isdir(os.path.join(event_dir,"cache")):
				has_cache = True
			else:
				has_cache = False
			# Execute gw_data_find.
			if has_cache:
				for ifo in ifos:
					copyfile(os.path.join(event_dir,"cache/%s.cache"%ifo),os.path.join(cache_dir,"%s.cache"%ifo))
			else:
				for i in range(len(ifos)):
					cmd = "gw_data_find --observatory %s --type %s --gps-start-time %s --gps-end-time %s --output-file '%s' --lal-cache -u file"%(
							ifos[i][0],
							frames[ifos[i]],
							int(trigtime-64),
							int(trigtime+64),
							os.path.join(cache_dir,"%s.cache"%(ifos[i],)))
					os.system(cmd)
			job_res_arg = "--li-samples %s --input-channel %s --use-maxL --ifos %s --frame-cache-path %s --srate %s --duration %s --trigtime %.6f --output-path %s --make-plots --output-channel residual --frame-type HOFT_RES --make-omega-scans --calibrate --choose-fd --approx %s "%(pe_file," ".join(channels_arr)," ".join(ifos),cache_dir,int(float(srate)),seglen,trigtime,residual_dir,approx)
			job_res_arg += " --psd-files %s"%(" ".join([os.path.join(event_dir,"PSD","%s_PSD.dat"%(ifo)) for ifo in ifos]))
			job_res = Job(
					name="residual_%s_%s"%(event,'Prod0'),
					executable="%s"%(os.popen("which gwcomp_residuals.py").read().strip()),
					error=error,
					output=output,
					log=log,
					submit=submit,
					request_memory=config["residual"]["request-memory"],
					request_disk=config["residual"]["request-disk"],
					getenv=True,
					universe=config["condor"]["universe"],
					extra_lines=extra_lines,
					dag=dag_res)
			job_res.add_arg(str(job_res_arg))
			#command = "gwcomp_residuals.py "+job_res_arg
			#out, err = res_proc.communicate()
			res_cache = {}
			for ifo in ifos:
				res_cache[ifo] = os.path.join(bw_res_cache_dir,"%s.cache"%ifo)
			config_bw_res.set("datafind", "cache-files", str(res_cache))
			with open(os.path.join(rundir,"bayeswave_residual_config.ini"),"w") as f:	
				config_bw_res.write(f)
			with open(os.path.join(rundir,"bayeswave_residual.sh"),"w") as f:
				f.write(bw_res_bash_template.substitute(workdir=bw_res_dir,trigtime="%.6f"%trigtime,config=os.path.join(rundir,"bayeswave_residual_config.ini"), 
					rundir='%s_bayeswave_residual'%event))
			cache = {}
			for ifo in ifos:
				cache[ifo] = os.path.join(rundir,"cache/%s.cache"%ifo)
			config_bw_event.set("datafind", "cache-files", str(cache))
			extra_run_options = ''
			if has_cache:
				config_bw_event.set("datafind", "ignore-science-segments", "True")
				extra_run_options = '--skip-datafind --copy-frames'
			with open(os.path.join(rundir,"bayeswave_event_config.ini"),"w") as f:
				config_bw_event.write(f)
			with open(os.path.join(rundir,"bayeswave_event.sh"),"w") as f:
				f.write(bw_event_bash_template.substitute(workdir=bw_event_dir,trigtime="%.6f"%trigtime,config=os.path.join(rundir,"bayeswave_event_config.ini"),
					rundir='%s_bayeswave_event'%event, extra=extra_run_options))
			os.chmod(os.path.join(rundir,"bayeswave_residual.sh"),stat.S_IRWXU)
			os.chmod(os.path.join(rundir,"bayeswave_event.sh"),stat.S_IRWXU)
			for ifo in ifos:
				bw_run_f.write("ls \"%s/%s\"*.gwf | lalapps_path2cache > \"%s/%s.cache\"\n"%(residual_dir,ifo[0],bw_res_cache_dir,ifo))
			bw_run_f.write(". \"%s\"\n"%os.path.join(rundir,"bayeswave_residual.sh"))
			bw_run_f.write(". \"%s\"\n"%os.path.join(rundir,"bayeswave_event.sh"))
		if opts.submit:
			dag_res.build_submit()
		else:
			dag_res.build()
		bw_run_f.close()
		os.chmod("bayeswave_run.sh",stat.S_IRWXU)
		# Print out all issues.
		if len(ISSUES.values())>0:
			cprint("##########","yellow")
			cprint("# ISSUES #","yellow")
			cprint("##########","yellow")
			cprint("The following superevent(s) is/are missing in %s:"%pe_data_dir,"yellow")
			if len(ISSUES["missing-PEs"])==0:
				cprint("---","yellow")
			else:
				for ele in ISSUES["missing-PEs"]:
					cprint(ele,"yellow")
