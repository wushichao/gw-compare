##################################################################################
# Copyright (C) 2020 Isaac Chun Fung Wong
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##################################################################################

##
# @file gwcomp_bw_background_pipe.py
# @author Isaac Chun Fung Wong
# @brief This program generate a bash scrip to perform BayesWave background run.

from __future__ import print_function
import argparse
from string import Template

bw_bash_template = """#!/bin/bash
#image=\"/cvmfs/singularity.opensciencegrid.org/lscsoft/bayeswave:latest\"
#SINGULARITYENV_X509_USER_PROXY=$$(grid-proxy-info -path)
#singularity_cmd=\"SINGULARITYENV_X509_USER_PROXY=$$(grid-proxy-info -path) singularity exec --bind /local --bind /hdfs $${image}\"
source /home/sudarshan.ghonge/opt/bayeswave_master/etc/bayeswave-user-env.sh

cmdline=\"bayeswave_pipe -r $workdir --bayesline-median-psd $args $config --osg-deploy --skip-megapy\"

echo \"Executing:\"
echo \"$${cmdline}\"
eval \"$${cmdline}\"
condor_submit_dag $workdir/$rundir.dag
"""
bw_bash_template = Template(bw_bash_template)

def parser():
	parser = argparse.ArgumentParser(description="This program generates a bash script to perform BayesWave background run.")
	parser.add_argument("--get",type=str,help="Path to generate a config file.",required=False)
	parser.add_argument("--config",type=str,help="Path to a completed config file.",required=False)
	parser.add_argument("--submit",help="Submit the job",action="store_true")
	return parser

def main(args=None):
	from configparser import ConfigParser
	import os

	opts = parser().parse_args(args)

	if opts.get is not None:
		config = ConfigParser(allow_no_value=True)
		config.optionxform = str
		config.add_section("condor")
		config.set("condor","accounting-group-user",os.popen("whoami").read().strip())
		config.set("condor","universe","vanilla")
		config.add_section("segments")
		config.set("segments","# Path to a file containing analysis segments.")
		config.set("segments","analysis-segments","{\"H1\": [], \"L1\": [], \"V1\": []}")
		config.set("segments","# Path to a file containing veto segments.")
		config.set("segments","veto-segments","{\"H1\": [], \"L1\": [], \"V1\": []}")
		config.add_section("events")
		config.set("events","# Superevent IDs separated by space.")
		config.set("events","superevents","")
		config.add_section("injections")
		config.set("injections","# Left time window around trigger time in second.")
		config.set("injections","left-time-window","2048")
		config.set("injections","# Right time window around trigger time in second.")
		config.set("injections","right-time-window","2048")
		config.set("injections","# Number of injections.")
		config.set("injections","ninject","200")
		config.set("injections","# Injection type. Supported: \"maxL\", \"fair-draw\".")
		config.set("injections","inj-type","fair-draw")
		config.set("injections","# Seed for generating a random sequence of trigtimes.")
		config.set("injections","seed","123")
		config.add_section("bayeswave")
		config.set("bayeswave","accounting-group","ligo.prod.o3.burst.paramest.bayeswave")
		config.set("bayeswave","request-memory","6000")
		config.set("bayeswave","post-request-memory","12000")
                config.set("bayeswave","bayeswave","/bin/BayesWave")
                config.set("bayeswave","bayeswave_post","/bin/BayesWavePost")
                config.set("bayeswave","megaplot", "/bin/megaplot.py")
                config.set("bayeswave","megasky","/bin/megasky.py")
                config.set("bayeswave","singularity","\"/cvmfs/singularity.opensciencegrid.org/lscsoft/bayeswave:v1.0.5\"")
		with open(opts.get,"w") as f:
			config.write(f)
	elif opts.config is not None:
		import stat
		import numpy as np
		import ast
		import json
		from termcolor import cprint
		import ligo.segments
		from ligo.gracedb.rest import GraceDb
                import glob

		config = ConfigParser()
		config.optionxform = str
		config.read(opts.config)
		analysis_segment_files = ast.literal_eval(config["segments"]["analysis-segments"])
		veto_segment_files = ast.literal_eval(config["segments"]["veto-segments"])
		analysis_segments = {ifo: ligo.segments.segmentlist([ligo.segments.segment(x,y) for seg_file in analysis_segment_files[ifo] for x,y in np.atleast_2d(np.loadtxt(seg_file))]) for ifo in analysis_segment_files.keys()}
		veto_segments = {ifo: ligo.segments.segmentlist([ligo.segments.segment(x,y) for seg_file in veto_segment_files[ifo] for x,y in np.atleast_2d(np.loadtxt(seg_file))]) for ifo in veto_segment_files.keys()}
		clean_segments = {ifo: analysis_segments[ifo]-veto_segments[ifo] for ifo in analysis_segments.keys()}
		left_time_window = float(config["injections"]["left-time-window"])
		right_time_window = float(config["injections"]["right-time-window"])
		ninject = int(config["injections"]["ninject"])
		# Default sampling rate.
		srate_max = 2048
		# Maximum seglen.
		seglen_max = 8
		# Directory which stores the PE data.
		pe_dir = "/home/sudarshan.ghonge/Projects/O3_PE/o3a_catalog_events"
                pe_dat_dir = '/home/sudarshan.ghonge/Projects/O3a/C01_runs/' 
		superevents = config["events"]["superevents"].split()

		PWD = os.getcwd()
		# A BayesWave config file template.
		config_bw = ConfigParser()
		config_bw.optionxform = str
		config_bw.add_section("input")
		config_bw.set("input","dataseed","1234")
		config_bw.set("input","seglen","")
		config_bw.set("input","window","1.0")
		config_bw.set("input","flow","20")
		config_bw.set("input","srate","")
		config_bw.set("input","PSDlength","")
		config_bw.set("input","padding","0.0")
		config_bw.set("input","keep-frac","1.0")
		config_bw.set("input","rho-threshold","7.0")
		config_bw.set("input","ifo-list","")
		config_bw.add_section("engine")
		config_bw.set("engine","bayeswave",config["bayeswave"]["bayeswave"])
		config_bw.set("engine","bayeswave_post",config["bayeswave"]["bayeswave_post"])
		config_bw.set("engine","megaplot",config["bayeswave"]["megaplot"])
		config_bw.set("engine","megasky",config["bayeswave"]["megasky"])
		config_bw.set("engine","singularity",config["bayeswave"]["singularity"])
		config_bw.add_section("datafind")
		config_bw.set("datafind","frtype-list","")
		config_bw.set("datafind","channel-list","")
		config_bw.set("datafind","url-type","file")
		config_bw.set("datafind","veto-categories","[1]")
		config_bw.add_section("bayeswave_options")
		config_bw.set("bayeswave_options","updateGeocenterPSD","")
		config_bw.set("bayeswave_options","waveletPrior","")
		config_bw.set("bayeswave_options","Dmax","100")
		config_bw.set("bayeswave_options","signalOnly","")
		config_bw.set("bayeswave_options","inj-fref","20")
		config_bw.add_section("bayeswave_post_options")
		config_bw.set("bayeswave_post_options","0noise","")
		config_bw.set("bayeswave_post_options","lite","")
		config_bw.set("bayeswave_post_options","inj-fref","20")
		config_bw.add_section("injections")
		config_bw.set("injections","events","all")
		config_bw.set("injections","inj-fref","20")
		config_bw.add_section("condor")
		config_bw.set("condor","universe",config["condor"]["universe"])
		config_bw.set("condor","checkpoint","")
		config_bw.set("condor","bayeswave-request-memory",config["bayeswave"]["request-memory"])
		config_bw.set("condor","bayeswave_post-request-memory",config["bayeswave"]["post-request-memory"])
		config_bw.set("condor","datafind","/usr/bin/gw_data_find")
		config_bw.set("condor","ligolw_print","/usr/bin/ligolw_print")
		config_bw.set("condor","segfind","/usr/bin/ligolw_segment_query_dqsegdb")
		config_bw.set("condor","accounting-group",config["bayeswave"]["accounting-group"])
		config_bw.set("condor","accounting-group-user",os.popen("whoami").read().strip())
		config_bw.add_section("segfind")
		config_bw.set("segfind","segment-url","https://segments.ligo.org")
		config_bw.add_section("segments")
		config_bw.set("segments","h1-analyze","H1:DMT-ANALYSIS_READY:1")
		config_bw.set("segments","l1-analyze","L1:DMT-ANALYSIS_READY:1")
		config_bw.set("segments","v1-analyze","V1:ITF_SCIENCEMODE")

		# A bash script to execute all runs.
		bw_bash_fp = open("bayeswave_run.sh","w")
		bw_bash_fp.write("#!/bin/bash\n\n")
		client = GraceDb()

		# Seed the numpy RNG.
		np.random.seed(int(config["injections"]["seed"]))

		for superevent in superevents:
			cprint("Processing %s..."%superevent,"yellow")
			# Create directory.
			if not os.path.exists(superevent):
					os.mkdir(superevent)

                        pe_config_file  = glob.glob(os.path.join(pe_dir, superevent, 'C01_offline', 'Prod0*.ini'))[0]
                        config_pe = ConfigParser()
                        config_pe.optionxform = str
                        config_pe.read(pe_config_file)
                        ifos = ast.literal_eval(config_pe["analysis"]["ifos"])
			trigtime_file = glob.glob(os.path.join(pe_dir, superevent, "C01_offline", "*gps*.txt".format(superevent)))
                        trigtime = np.asscalar(np.loadtxt(trigtime_file[0]))
			start_time = trigtime-left_time_window
			end_time = trigtime+right_time_window
			segments_event = clean_segments[ifos[0]]
			for i in range(1,len(ifos)):
				segments_event = segments_event&clean_segments[ifos[i]]
                        seglen = config_pe.getfloat("engine","seglen")
                        if seglen > seglen_max:
                                cprint("WARNING: seglen (%s) in PE config is greater than the default maximum (%s). seglen is set to the default maximum value."%(seglen,seglen_max),"red")
				seglen = seglen_max
                        srate = config_pe.getfloat("engine","srate")
                        if srate>srate_max:
                                cprint("WARNING: srate (%s) in PE config is greater than the default maximum (%s). srate is set to the default maximum value."%(srate,srate_max),"yellow")
				srate = srate_max
                        # Get frame types and channels.
                        frames = ast.literal_eval(config_pe["datafind"]["types"])
                        frames = {ifo:frames[ifo] for ifo in ifos}
                        # Regulate the frame types. We don't need glitch cleaned frames for background
                        for ifo in ifos:
                                if "%s_HOFT_C01"%ifo in frames[ifo]:
                                        if ifo=="H1":
                                                frames[ifo] = "H1_HOFT_C01"
                                        elif ifo=="L1":
                                                frames[ifo] = "L1_HOFT_C01"
                                elif ifo=="V1":
                                        frames[ifo] = "V1Online"

                        channels = ast.literal_eval(config_pe["data"]["channels"])
                        channels = {ifo:channels[ifo] for ifo in ifos}
                        # Regulate channel names. We don't need glitch cleaned frames for background
                        for ifo in ifos:
                                if "%s:DCS-CALIB_STRAIN_CLEAN_C01"%ifo in channels[ifo]:
                                        if ifo=="H1":
                                                channels[ifo] = "H1:DCS-CALIB_STRAIN_C01"
                                        elif ifo=="L1":
                                                channels[ifo] = "L1:DCS-CALIB_STRAIN_C01"
                                elif ifo=="V1":
                                        channels[ifo] = "V1:Hrec_hoft_16384Hz"


			# Get the trigtimes.
                        trigtimes_event_path = os.path.join(pe_dat_dir,superevent,"trigtimes.txt")
                        if(not os.path.exists(trigtimes_event_path)):
                                cprint("Background trigtimes do not exist. Creating new files...", "blue")
                                segment_list = ligo.segments.segmentlist([])
                                while len(segment_list)<ninject:
                                        rand_time = np.random.uniform(start_time,end_time,1)
                                        rand_seg = ligo.segments.segment(rand_time-seglen/2.,rand_time+seglen/2.)
                                        if (rand_seg in segments_event) and (not segment_list.intersects_segment(rand_seg)):
                                                segment_list.extend([rand_seg])
                                # Get the trigtimes.
                                trigtimes_event_path = os.path.join(PWD,superevent,"trigtimes.txt")
                                rand_trigtimes = np.array([(seg[0][0]+seg[1][0])/2. for seg in segment_list])
                                rand_trigtimes = np.sort(rand_trigtimes)
                                trigtimes_event_path = os.path.join(PWD,superevent,"trigtimes.txt")
                                np.savetxt(trigtimes_event_path,rand_trigtimes,"%.6f")
			# Get injection table.
			inj_file_root = os.path.join(PWD,superevent,"%s_injection"%(superevent))
			os.system("gwcomp-generate-injection-table-from-samples --pefile %s --configfile %s --trigtime-list \"%s\" --inj-type %s --root %s"%(
					os.path.join(pe_dat_dir, superevent,"PE", "pe_samples.dat"), pe_config_file, trigtimes_event_path,config["injections"]["inj-type"],inj_file_root))
			# Config for catalog run.
                        flow=ast.literal_eval(config_pe["lalinference"]["flow"])
                        flow = np.min([float(flow[ifo]) for ifo in ifos])
                        config_bw.set("input", "flow", str(flow))
                        fref = float(config_pe["engine"]["fref"])
                        config_bw.set("bayeswave_options", "inj-fref", str(fref))
                        config_bw.set("bayeswave_post_options", "inj-fref", str(fref))
			# In principle the config of all EXPs should be the same, but just in case.
			config_bw.set("input","seglen",str(seglen))
                        config_bw.set("input","srate",str(srate))
			config_bw.set("input","ifo-list",str(ifos))
			config_bw.set("input","PSDlength",str(seglen))
			config_bw.set("datafind","frtype-list",str(frames))
			config_bw.set("datafind","channel-list",str(channels))
			config_event_path = os.path.join(PWD,superevent,"bayeswave_catalog_offsource_config.ini")
                        if(seglen*srate>2048.0*4.0):
                                config_bw.set("condor","bayeswave-request-memory",str(float(config["bayeswave"]["request-memory"])*seglen*srate/(2048.*4.0)))
                                config_bw.set("condor","bayeswave_post-request-memory",str(float(config["bayeswave"]["post-request-memory"])*seglen*srate/(2048.*4.0)))
			bash_catalog_event_path = os.path.join(PWD,superevent,"bayeswave_catalog_offsource.sh")
			with open(config_event_path,"w") as f:
				config_bw.write(f)
			# Write bash script.
			with open(bash_catalog_event_path,"w") as f:
				f.write(bw_bash_template.substitute(workdir=os.path.join(PWD,superevent,"%s_catalog_run"%(superevent)),args="-I %s"%(inj_file_root+".xml"),config=config_event_path, rundir="%s_catalog_run"%superevent))
			os.chmod(bash_catalog_event_path,0o744)
			#bw_bash_fp.write(". \"%s\"\n"%bash_catalog_event_path)
			#bw_bash_fp.close()
			#os.chmod("bayeswave_catalog_offsource.sh",0o744)
