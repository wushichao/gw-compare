#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Install script for package"""

try:
	from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

setup(
    name='gw_compare',
    description='Utilities for waveform reconstruction comparisons',
    version='0.1',
    author='James Alexander Clark, Sudarshan Ghonge',
    author_email='james.clark@ligo.org',
    url='https://git.ligo.org/james-clark/gw-compare',
    scripts=['scripts/gwcomp_driver.py', 'scripts/gwcomp_reclal.py',
             'scripts/gwcomp_residuals.py', 'scripts/gwcomp_testgr.py',
             'scripts/gwcomp_qscans.py', 'scripts/gwcomp_pos2siminspiral.py',
             'scripts/gwcomp_map2siminspiral.py', 'scripts/gwcomp_injtimes.py',
             'scripts/gwcomp_bw_li_inj.py', 'scripts/gwcomp_combinemedians.py'],
	packages=find_packages(),
	entry_points={
			"console_scripts":[
				"gwcomp-residual-pipe=gw_compare.tools.gwcomp_residual_pipe:main",
				"gwcomp-bw-background-pipe=gw_compare.tools.gwcomp_bw_background_pipe:main",
				"gwcomp-bw-catalog-pipe=gw_compare.tools.gwcomp_bw_catalog_pipe:main",
				"gwcomp-bw-onsource-pipe=gw_compare.tools.gwcomp_bw_onsource_pipe:main",
				"gwcomp-generate-injection-table-from-samples=gw_compare.tools.gwcomp_generate_injection_table_from_samples:main",
				"gwcomp-reclal-pipe=gw_compare.tools.gwcomp_reclal_pipe:main",
				"gwcomp-bw-li-inj-pipe=gw_compare.tools.gwcomp_bw_li_inj_pipe:main"
			]
	},
    #packages=['gw_compare']
)
